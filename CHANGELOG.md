# 1.0.0 (2019-11-07)


### Bug Fixes

* Update variable for changes in bare variable ([4e59451](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/4e59451))
